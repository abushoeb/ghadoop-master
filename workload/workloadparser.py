#!/usr/bin/env python

from operator import itemgetter

jobs = {}
jobTime = []
jobsStart = {}

tasks = {}
taskTime = {}

# http://code.google.com/p/googleclusterdata/
fd = open("google-cluster-data-1.csv")
content = fd.readline()
while (content != "" ):
	if not content.startswith("Time"):
		# Parse line
		content = content.replace( "\n", "" ).split(" ")
		# Time ParentID TaskID JobType NrmlTaskCores NrmlTaskMem
		time = int(content[0])
		jobId = content[1]
		taskId = content[2]
		
		if time not in taskTime:
			taskTime[time] = 0
		taskTime[time] += 1
		
		if jobId not in jobs:
			jobs[jobId] = []
			jobsStart[jobId] = time
			jobTime.append(jobId)
		if taskId not in jobs[jobId]:
			jobs[jobId].append(taskId)
		if taskId not in tasks:
			tasks[taskId] = []
		tasks[taskId].append(time)
	
	# Next line
	content = fd.readline()

jobTasks = []
lenTasks = {}

for jobId in jobTime:
	print jobId+" @ "+str(jobsStart[jobId])+" => tasks=" +str(len(jobs[jobId]))
	jobTasks.append((jobId, len(jobs[jobId])))
	for taskId in jobs[jobId]:
		start = tasks[taskId][0]
		end = tasks[taskId][len(tasks[taskId])-1]
		print "\t"+taskId+": "+str(start)+"-"+str(end)+" = "+str(end-start)+"s"
		length = end-start
		if not length in lenTasks:
			lenTasks[length] = 0
		lenTasks[length] += 1

numTasks = {}
for tuple in sorted(jobTasks, key=itemgetter(1)):
	if tuple[1] not in numTasks:
		numTasks[tuple[1]] = 0
	numTasks[tuple[1]] += 1
	print tuple

print "Number of jobs with a given number of tasks:"
for num in sorted(numTasks):
	print str(num)+" tasks => "+str(numTasks[num])+" jobs"


print "Number of jobs with a given:"
for length in sorted(lenTasks):
	print str(length)+" s => "+str(lenTasks[length])+" jobs"

print "Number of tasks over time:"
for time in sorted(taskTime):
	print str(time)+" "+str(taskTime[time])

print "Job submission"
startJob={}
for jobId in jobsStart:
	start = jobsStart[jobId]
	if start not in startJob:
		startJob[start] = 0
	startJob[start]+=1
for time in sorted(startJob):
	print str(time)+" "+str(startJob[time])

print "Number of jobs:"
print len(jobs)

