#!/usr/bin/env python

from operator import itemgetter

jobs = {}
jobTime = []
jobsStart = {}

tasks = {}
taskTime = {}

typeTasks = { 0: 4, 1: 10, 2: 20, 3: 40, 4: 80, 5: 150, 6: 300, 7: 600, 8: 1600 }

# Generate data
fd = open("workload1-pre")
content = fd.readline()
jobId=0
numTasks=0
while (content != "" ):
	if not content.startswith("#"):
		# Parse line
		content.replace("\n", "")
		time = int(content.split("\t")[0])
		type = int(content.split("\t")[1])
		numTasks = typeTasks[type]
		
		id = str(jobId).zfill(2)
		
		#print id+"\t"+str(time)+"\t"+str(type)+"\t"+str(typeTasks[type])
		# 10m30	NORMAL		/user/goiri/input20	/user/goiri/output20	jar /home/goiri/hadoop-0.21.0/hadoop-mapred-examples-0.21.0.jar wordcount
		
		# Create files
		print "/home/goiri/hadoop-0.21.0/bin/hadoop fs -mkdir /user/goiri/input-workload1-"+id 
		for i in range(0, numTasks):
			num = str(i).zfill(4)
			print "/home/goiri/hadoop-0.21.0/bin/hadoop fs -cp /user/goiri/testfile /user/goiri/input-workload1-"+id+"/file"+num
		
		numTasks+=typeTasks[type]
		
		jobId+=1
	
	# Next line
	content = fd.readline()


fd = open("workload1-pre")
content = fd.readline()
i=0
numTasks=0
while (content != "" ):
	if not content.startswith("#"):
		# Parse line
		content.replace("\n", "")
		time = int(content.split("\t")[0])
		type = int(content.split("\t")[1])
		numTasks = typeTasks[type]
		
		id = str(i).zfill(2)
		
		#print id+"\t"+str(time)+"\t"+str(type)+"\t"+str(typeTasks[type])
		# 10m30	NORMAL		/user/goiri/input20	/user/goiri/output20	jar /home/goiri/hadoop-0.21.0/hadoop-mapred-examples-0.21.0.jar wordcount
		
		# Create files
		print str(time)+"\tNORMAL\t/user/goiri/input-workload1-"+id+"\t/user/goiri/output-workload1-"+id+"\tjar /home/goiri/hadoop-0.21.0/hadoop-mapred-examples-0.21.0.jar wordcount"
		
		numTasks+=typeTasks[type]
		
		i+=1
	
	# Next line
	content = fd.readline()

#print numTasks
#print ((numTasks*34.5)/16/3.0)/3600.0
