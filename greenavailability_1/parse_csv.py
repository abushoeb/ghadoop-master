#!/usr/bin/python

import os, fnmatch, tempfile, sys,os.path,pickle
from datetime import *

fileformat = "%b_%d_%Y.txt"

def getday(pday, datapath='.'):
	prod = []
	hours = []
	filename = os.path.join(datapath,"parsedHourly/"+pday.strftime(fileformat))
	if not os.path.isfile(filename):
		print "No forecast found ", filename
		return prod
	fd = open(filename,'r')
	lines = fd.readlines()
	for i in range(1,len(lines)):
		list1=lines[i].strip().split(",")
		hours.append(list1[0])
		prod.append(float(list1[4]))
	if len(prod)<24:
		print "not enough data. we have these hour(s) ", hours

	return prod

def process(sday, num_hours, datapath='.'):
	retval = []
	tday = sday
	daydelta = timedelta(days=1)
	len_retval = num_hours
	while num_hours>0:
		result = getday(sday,datapath)
		#print result
		retval.extend(result[sday.hour:24])
		num_hours -= (24-sday.hour)
		nday = datetime(sday.year,sday.month,sday.day,0,0)
		sday = nday+daydelta
		#print retval
	return retval[0:len_retval]

if __name__ == '__main__':
	#now = datetime.now()
	now = datetime(2011, 6, 3, 9, 0,0)
	result = process(now, 48)
	print result
