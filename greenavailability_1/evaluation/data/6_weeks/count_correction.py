#!/usr/bin/python

import os,sys,re,string,getopt,glob,math
from datetime import *


def process(filename):
	
	fd = open(filename,'r')
	cin = 0
	cpossible = 0
	ctotal = 0
	cnonzero = 0

	for line in fd:
		line = line.strip()
		if not line or line.startswith("#"):
			continue
		
		elements = line.split()
		startTime = elements[0]+" "+elements[1]

		hour = int(elements[3])
		pred = float(elements[6])
		actual = float(elements[7])
		base = float(elements[8])
		tag = float(elements[9])
		
		ctotal += 1
		if base > 0.0:
			cnonzero += 1
			if hour > 8:
				cpossible += 1
				if abs(pred-base*tag/100) > 2.0:
					#print startTime, pred, base, pred-base*tag/100, elements[12]
					cin += 1
				#elif hour<17:
					#print startTime, pred, base, pred-base*tag/100, elements[12]

	print filename, cin, cpossible, cnonzero, ctotal

	return
			
if __name__=='__main__':

	commonOptions="f:d:hl"
	opts, args = getopt.getopt(sys.argv[1:], commonOptions)
	printHtml=False
	twodays = True
	files = []
    
	for o,a in opts:
		if o == '-b':
			a = a.strip()
		elif o == '-d':        
			dirname = a
			files.extend(glob.glob(os.path.join(dirname,"*.txt")))            
		elif o == '-f':
			files.extend(a.split(","))

	files.sort()
	for f in files:
		process(f)

