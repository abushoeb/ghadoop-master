#!/usr/bin/python

import sys,os.path
import math,subprocess
from datetime import timedelta, datetime
import getopt
sys.path.append(r'greenavailability/')
import model,setenv
import get_real_data
import gslurmcommons  

"""
can be used to call model for longer period of time and will generate ouput file per call/day
"""

setenv.init()


format = "%d-%m-%Y"
dateWorst = datetime(2011, 1, 24, 9, 0, 0) # Fewer energy OK
dateWorstUs = datetime(2010, 8, 23, 9, 0, 0) # Worst for us
dateBest = datetime(2010, 5, 31, 9,0 , 0) # More energy
dateBestUs = datetime(2010, 11, 1, 9, 0, 0) # Best for us OK TODO
dateAverage = datetime(2010, 10, 11, 9, 0, 0) # Best for us OK TODO

BASE_DATE = dateWorstUs
MAX_POWER = 1347#2300
NUMBER_OF_HOURS_IN_ONE_DAY=24
useActual = False
BASE_POWER = 1347
prefix=""
horizon = 1
#actuaData = {dateWorst: "data/solarpower-24-01-2011", dateWorstUs: "data/solarpower-23-08-2010", dateBest: "data/solarpower-31-05-2010", dateBestUs: "data/solarpower-01-11-2010", dateAverage: "data/solarpower-11-10-2010"}
numberOfDays = 5
commonOptions="b:d:gap:Im:h:"
opts, args = getopt.getopt(sys.argv[1:], commonOptions)

genGraphs = False

useInigoActual = True

noofmethods = 3


for o,a in opts:
    if o == '-b':
        a = a.strip()
        d = datetime.strptime(a,format)
        BASE_DATE = datetime(d.year,d.month,d.day)
    elif o == '-d':
        numberOfDays = int(a)
    elif o == '-g':
        genGraphs = True
    elif o == '-a':
        useActual = True
    elif o == '-I':
        useInigoActual = False
    elif o == '-m':
        MAX_POWER = float(a)
    elif o=='-p':
        prefix = a.strip()
    elif o=='-h':
        horizon = int(a.strip())

#actuaData = "data/solarpower-%s"%(BASE_DATE.strftime(format))
numSlots = numberOfDays*NUMBER_OF_HOURS_IN_ONE_DAY
SLOTLENGTH=3600

dirname = "debug"
#dirname = []
#for j in range(noofmethods):
	#dirname.append("debug%d"%j)
#	dirname.append("debug")



def generateGraph(prefix,filename,fd,d):
	#print filename

        print >>fd,"set terminal png transparent nocrop font arial 14 size 1920,600" 
        print >>fd,"set output 'day%s_%s.png'"%(prefix,filename)
        print >>fd,"""
set boxwidth 1 absolute
set style fill solid 1 border -1
set style histogram clustered gap 1 title  offset character 0, 0, 0
set datafile missing '-'
set style data histograms
set xtics border in scale 1,0.5 nomirror  norotate offset character 0, 0, 0 
#set xtics   ("1891-1900" 0.00000, "1901-1910" 1.00000, "1911-1920" 2.00000, "1921-1930" 3.00000, "1931-1940" 4.00000, "1941-1950" 5.00000, "1951-1960" 6.00000, "1961-1970" 7.00000)
set y2range [*:*]
set ytics nomirror
set x2tics 1
set y2tics 100 nomirror
#set yrange [ 0.00000 : 300000. ] noreverse nowriteback"""
        print >>fd,"set title '%s'"%(d.strftime("%m/%d/%Y %H"))
        print >>fd,"plot 'debug/data_%s.txt' using 5:xtic(3):xticlabels(4) ti \"Pred1\", '' u 6 ti \"Pred2\", '' u 7 ti \"Pred3\", '' u 8 ti \"Actual\",'' u 9 ti 'Base','' u 0:($5-$8) axis x1y2 t 'diff' w lp, '' u ($0):5:($15) with labels center offset 0,1 notitle"%(filename)
        print >>fd


        print >>fd,"set terminal png transparent nocrop font arial 14 size 1920,600" 
        print >>fd,"set output 'day%s_%s_tag.png'"%(prefix,filename)
        print >>fd,"""
set boxwidth 1 absolute
set style fill solid 1 border -1
set style histogram clustered gap 1 title  offset character 0, 0, 0
set datafile missing '-'
set style data histograms
set xtics border in scale 1,0.5 nomirror  rotate by -45 offset character 0, 0, 0 
#set xtics   ("1891-1900" 0.00000, "1901-1910" 1.00000, "1911-1920" 2.00000, "1921-1930" 3.00000, "1931-1940" 4.00000, "1941-1950" 5.00000, "1951-1960" 6.00000, "1961-1970" 7.00000)
set ytics nomirror
set xtics mirror
set x2tics 1
#set y2range [*:*]
#set y2tics 100 nomirror
#set yrange [ 0.00000 : 300000. ] noreverse nowriteback"""
        print >>fd,"set title '%s tags'"%(d.strftime("%m/%d/%Y %H"))
        print >>fd,"plot 'debug/data_%s.txt' using 10:xtic(3):xticlabels(13) ti \"Tag\", '' u ($8/$9*100) ti \"Actual\""%(filename)
        print >>fd
def generatePlotFile():
    global numberOfDays,dirname,BASE_DATE,prefix,horizon
    
    print "generating plots"

    
    gpFilename = "plot.gp"
    fd = open(gpFilename,'w')
    
    for day in range(numberOfDays):
        d = BASE_DATE + timedelta(days=day)

	if horizon == 1:
            filename = "%d"%(day)
            generateGraph(prefix,filename,fd,d)
	else:
            numberOfHoursInDay = NUMBER_OF_HOURS_IN_ONE_DAY
            #if day == 0:
        	#numberOfHoursInDay = NUMBER_OF_HOURS_IN_ONE_DAY-9
        
            for hour in range(numberOfHoursInDay):            
		filename = "%d_%02d"%(day,hour)
                generateGraph(prefix,filename,fd,d+timedelta(hours=hour))
            




    #cmd = "gnuplot %s"%(gpFilename)
    #print cmd
    #os.system(cmd)
    
    #try:
    #    retcode = subprocess.call(cmd, shell=True)
    #    if retcode < 0:
    #        print >>sys.stderr, "Child was terminated by signal", -retcode
    #    else:
    #        print >>sys.stderr, "Child returned", retcode
    #except OSError, e:
    #    print >>sys.stderr, "Execution failed:", e

# Get the available green power
def readGreenAvailFile(filename):
    greenAvailability = []
    file = open(filename, 'r')
    for line in file:
        if line != '' and line.find("#")!=0 and line != '\n':
            lineSplit = line.strip().expandtabs(1).split(' ')
            t=lineSplit[0]
            p=float(lineSplit[1])
            greenAvailability.append(gslurmcommons.TimeValue(t,p))
    file.close()
    return greenAvailability

def readGreenAvailData(BASE_DATE, numSlots):
	greenAvailability = []
	alldata = get_real_data.process(BASE_DATE, numSlots)
	for i in range(len(alldata)):
		greenAvailability.append(alldata[i])
	return greenAvailability

if genGraphs:
    generatePlotFile()
    sys.exit()

# Get actual from file
greenAvailabilityActual = readGreenAvailData(BASE_DATE, numSlots+horizon)
#greenAvailabilityActual = get_real_data.process(BASE_DATE, numSlots)
# Predictor

# Predictor
#if useCache:
#	ep = model.CachedEnergyPredictor(BASE_DATE,predictionHorizon=TOTALTIME/3600,path='./greenavailability',threshold=2, scalingFactor=MAX_POWER,scalingBase=BASE_POWER,debugging=True) # Threshold = 2hour; Capacity=2037W
#else:
#	ep = model.EnergyPredictor('./greenavailability', 2, MAX_POWER,scalingBase=BASE_POWER,debugging=True) # Threshold = 2hour; Capacity=2037W
ep=[]
ep.append(model.EnergyPredictor('./greenavailability', 2, MAX_POWER,useActualData=useActual,scalingBase=BASE_POWER,debugging=True,error_exit=model.normal)) # Threshold = 2hour; Capacity=2037W
ep.append(model.EnergyPredictor('./greenavailability', 2, MAX_POWER,useActualData=useActual,scalingBase=BASE_POWER,debugging=True,error_exit=model.enter_on_thresh))
ep.append(model.EnergyPredictor('./greenavailability', 2, MAX_POWER,useActualData=useActual,scalingBase=BASE_POWER,debugging=True,error_exit=model.enter_on_error))
if len(ep)<>noofmethods:
	print "you forgot something"
	sys.exit(1)

if (len(greenAvailabilityActual) < numSlots):
    print len(greenAvailabilityActual),numSlots
    sys.exit(1) 

print len(greenAvailabilityActual),numSlots

# Array
greenAvailArray = []
greenAvailArrayActual = []
for i in range(numSlots):
    greenAvailArray.append(0)
    greenAvailArrayActual.append(0)



#for abc in range(1):
if not os.path.exists(dirname):
    os.makedirs(dirname)

t=0

#print "baseDate is ",BASE_DATE
for day in range(numberOfDays):
   
   
    if horizon==1:
	    filename = dirname+"/data_%d.txt"%(day)
            fd = open(filename,'w')    
   
    numberOfHoursInDay = NUMBER_OF_HOURS_IN_ONE_DAY
    #if day == 0:
    #    numberOfHoursInDay = NUMBER_OF_HOURS_IN_ONE_DAY-9
       
    for hour in range(numberOfHoursInDay):
   
        if horizon>1:	
	    filename = dirname+"/data_%d_%02d.txt"%(day,hour)
            fd = open(filename,'w')    

        timeElapsed = t*SLOTLENGTH
        
	       
	date = BASE_DATE + timedelta(seconds=timeElapsed)
	#print BASE_DATE, date, t
        
        greenAvail=[]
        for obj in range(noofmethods):
	        ga, flag = ep[obj].getGreenAvailability(date, horizon)
		greenAvail.append(ga)
        
	        #greenAvail, flag = ep.getGreenAvailability(date, 24)
        
	        # Manage data: put it in the green availability matrix
	#        greenAvailability = []
	        
	    #    basedate = date(date.year,date.month,date.hour)
	    #    for i in range(0, len(greenAvail)):
	    #        d = date + timedelta(hours=i)
	    #        if i>0:
	    #            d = datetime(d.year, d.month, d.day, d.hour)
	    #        time = gslurmcommons.toSeconds(d-date) + timeElapsed
	    #        value = greenAvail[i]
	    #        greenAvailability.append(gslurmcommons.TimeValue(time,value))
	    #        print " -> ",gslurmcommons.toTimeString(time)," v="+str(value), greenAvailabilityActual[t+i].v
	
		# if len(greenAvail)<horizon:
		# 	print "can't predict",date,len(greenAvail)
		# 	raise Exception()
	if len(greenAvail[0])<>len(greenAvail[1]) or len(greenAvail[0])<>len(greenAvail[2]):
		print "length unequal", date, h
		sys.exit(1)

	for h in range(len(greenAvail[0])):
	  	currentDate = date+timedelta(hours=h)
	  	index = t + h	
	        predictedValue = 0
	        actual = 0
	        if useInigoActual:
			try:
	                	actual = greenAvailabilityActual[index]
			except IndexError:
				print currentDate, t, h,index, len(greenAvailabilityActual)
	                    	raise Exception()
	        if len(greenAvail[0])>0:
	                try:
	                    predictedValue = greenAvail[0][h]
	                except IndexError:
	                    print h,t,len(greenAvail[0])
	                    raise Exception()
	
	                if not useInigoActual:
	                    actual = ep[obj].debug[h+ep[obj].shiftFactor].actual * ep[obj].debug[h+ep[obj].shiftFactor].scaling
	    #            predictedValue = ep.debug.prediction * ep.debug.scaling
	
	
	        #    print t, predictedValue == 0, greenAvailabilityActual[t]== 0, ep.debug.baseValue==0
	        #    print t, predictedValue, greenAvailabilityActual[t], ep.debug.baseValue
		if not (actual == 0):
	        	error = 0
	        if True:#not actual == 0:
	                   #error = round(math.fabs(predictedValue-actual)/actual*100)
	
	        	#print >>fd, currentDate,i,currentDate.hour,"%.2f"%(predictedValue),actual,ep[obj].debug[h+ep[obj].shiftFactor],"%d"%(error)
			print >>fd, currentDate,i,currentDate.hour,"%.2f"%(greenAvail[0][h]),"%.2f"%(greenAvail[1][h]),"%.2f"%(greenAvail[2][h]),actual,ep[obj].debug[h+ep[obj].shiftFactor]
	                i+=1
	t+=1
	#print t, currentDate
	if horizon>1:	
	        fd.close()
if horizon == 1:
	fd.close	
	    
    
generatePlotFile()    
