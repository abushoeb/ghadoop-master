#!/usr/bin/python

#import os,sys,time
from datetime import datetime,timedelta, date
import os,re,sys,glob,os.path,time,getopt, string

"""
keep in same directory with all_data.csv
will return real production data
"""


format = "%m-%d-%Y"
formathour = "%m-%d-%Y-%H-%M"
alldatafile = "all_data.csv"
start_date = datetime(2010,03,8,0)
col_rows = 168 #each col holds col_rows number of data

def process(date, num_hours):
	data = {}
	fd = open(alldatafile, 'r')	
	
	dif = date -start_date
	start_offset = dif.days*24+dif.seconds/3600
	end_offset = start_offset + num_hours

	start_col = start_offset/col_rows
	end_col = end_offset/col_rows

	row = 0
	for line in fd:
		line = line.strip()
		list1 = line.split("\t")
		if len(list1)<(end_col+1):
			print "We do not have data for offset ",end_col*col_rows
			sys.exit(1)
		for j in range(start_col,end_col+1):
			if row+j*col_rows>=start_offset and row+j*col_rows<end_offset:
				data[row+j*col_rows]=list1[j]
		row += 1


	if len(data)<>num_hours:
		print >>sys.stderr, date, len(data), num_hours

	values = []
	keys = sorted(data.keys())
	for i in keys:
		values.append(float(data[i].strip()))
		
	return values

if __name__ == '__main__':
	
	commonOptions="b:n:nh:h:d"
	opts, args = getopt.getopt(sys.argv[1:], commonOptions)

	d = "none"	
	n = 0	
	details = True


	for o,a in opts:
		if o == '-b':
			a = a.strip()
			try:
		        	d = datetime.strptime(a,format)
			except ValueError:
				try:
					d = datetime.strptime(a,formathour)
				except ValueError:
					print "The format of start date is -b mm-dd-yyyy or -b mm-dd-yyyy-HH-MM"
					sys.exit(1)	        
		elif o == '-h':
			print "Usage: [-b start_date(mm-dd-yyyy or mm-dd-yyyy-HH-MM) -n no. of days -nh no. of hours -d print with datetime -h print help]"
			sys.exit(0)
		elif o == '-n':
			a = a.strip()
			n += int(a)*24
		elif o == '-nh':
			a = a.strip()
			n += int(a)
		elif o == '-d':
			details = False

	if d=="none":
		print "Usage: [-b start_date(mm-dd-yyyy or mm-dd-yyyy-HH-MM) -n no. of days -nh no. of hours -d print with datetime -h print help]"
		sys.exit(0)
	if n==0:
		n = 7*24 #defualt: return 1 week

	data = process(d,n)
	for i in range(len(data)):
		str1 = ""
		if details:
			str1 += str(d)+"\t"
			d += timedelta(hours=1)
		str1 += data[i]
		print str1
