#!/usr/bin/env python2.7

# Abu Shoeb, Computer Science, Rutgers University

import pipes, time

USER = "shoeb"
HADOOP_HOME = "/home/"+USER+"/hadoop-0.21.0"
PORT_MAPRED = 50060
PORT_HDFS = 50075
HADOOP_DECOMMISSION_MAPRED = HADOOP_HOME+"/conf/disabled_nodes"
HADOOP_DECOMMISSION_HDFS = HADOOP_HOME+"/conf/disabled_nodes_hdfs"
HADOOP_OFF_HDFS = HADOOP_HOME+"/conf/disabled_nodes_off"

print "Reading from pipefile"

for i in range(0,180): 
	print i
	result1 = open(HADOOP_DECOMMISSION_MAPRED).read()
	print "disabled_nodes\n"+result1
	
	result2 = open(HADOOP_DECOMMISSION_HDFS).read()
	print "disabled_nodes_hdfs\n"+result2
	
	result3 = open(HADOOP_OFF_HDFS).read()
	print "disabled_nodes_off\n"+result3
	time.sleep(0.9)
