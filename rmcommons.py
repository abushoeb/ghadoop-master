#!/usr/bin/python

# Abu Shoeb, Computer Science, Rutgers University
# This file contains all configurations and methods for resource manager

# Abu Shoeb modified on 2016-10-03 : FEATURE
SHARE_SLAVE_NODES = True
GC_REQ_SERVED = False
MAX_SHAREABLE_SLAVES = 4
CURRENT_SHARED_SLAVES = 0
ALREADY_SHARED_SLAVES = 0
SHARED_NODE_STATUS = 0 # 0 none, 1 dec, 2 off
SHARED_NODE_ID = "" # to do : make a list for multiple nodes 
SHARED_NODE_ID_LIST = [] # to do : make a list for multiple nodes
availableNodes = 0 

USER = "shoeb"
HADOOP_HOME = "/home/"+USER+"/hadoop-0.21.0"
GREEN_HADOOP_HOME = "/home/"+USER+"/ghadoop1"
GREEN_CASSANDRA_HOME = "/home/"+USER+"/gc1"
GC_REQ_FILE = GREEN_HADOOP_HOME+"/rmdata/gcreq"
GC_SIM_RES_FILE = GREEN_CASSANDRA_HOME+"/data/gc-sim-results.txt"
SHARED_NODE_FILE = "/home/"+USER+"/hadoop-0.21.0/conf/shared-nodes"

def writeGCreqToFile(noOfReqNode):
	#f = open(GC_REQ_FILE, 'a')
	f = open(GC_REQ_FILE, 'w')
	f.write(str(noOfReqNode))
	f.flush()
	#print "Write success"
	
def readGCreqFromFile():
	gcReq = open(GC_REQ_FILE).read()
	return int(gcReq)	

def writeGCsimResultToFile(current, future, nodes):
	f = open(GC_SIM_RES_FILE, 'a')
	result = str(current)+"\t"+str(future)+"\t"+str(nodes)+"\n"
	f.write(result)
	f.flush()
	#print "Write success"

# This method adds a Node ID to the file
def addNodeIdToFile(nodeId, fileName):
	f = open(fileName,"a")
	f.write(nodeId+'\n')
	f.close()

# This method removes a Node ID from the file
def removeNodeIdFromFile(nodeId, fileName):
	f = open(fileName,"r")
	lines = f.readlines()
	f.close()

	f = open(fileName,"w")
	for line in lines:
		if line!=nodeId+"\n":
			f.write(line)
	f.close()

def writeSharedNodeIdToFile(nodeId):
	f = open(SHARED_NODE_FILE, 'a')
	#f = open(GC_REQ_FILE, 'w')
	f.write(str(noOfReqNode))
	f.flush()
	#print "Write success"	
