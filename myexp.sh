#!/bin/bash

# Abu Shoeb modified
export HADOOP_HOME="/home/shoeb/hadoop-0.21.0"

function experiment {
	NAME=$1
	shift
	FLAGS=$*

	echo "name="$NAME" flags="$FLAGS

	# Abu Shoeb modified
	touch $HADOOP_HOME/logs/hadoop-jobtracker-crypt14.log
	touch $HADOOP_HOME/logs/hadoop-namenode-crypt14.log

	# Clean system
	echo "Cleanning..."
	cleaned=0
	retries=3
	while (( "$cleaned" == "0" )) && (( "$retries" > "0" )); do
		cleaned=1
#Abu Shoeb modified
#		./ghadoopclean.py > /dev/null &
 		./ghadoopclean.py &
		# > /dev/null &
		TESTPID=$!
		a=0
		while ps -p $TESTPID >/dev/null; do
			sleep 1
			let a=a+1
			if [ $a -gt 400 ]; then
				echo "Clean failed: try again..."
				kill -9 $TESTPID
				cleaned=0
				let retries=retries-1
			fi
		done
	done
	sleep 1
	echo "Starting..."

	rm -f logs/ghadoop-jobs.log
	rm -f logs/ghadoop-energy.log
	rm -f logs/ghadoop-scheduler.log
	rm -f logs/ghadoop-error.log
	touch logs/ghadoop-error.log
	sleep 1
	./ghadoopd $FLAGS

	# Save results
	mkdir -p logs/$NAME
# 	echo $NAME > logs/$NAME/$NAME-summary.log
	./ghadoopparser.py logs/ghadoop >> logs/$NAME/$NAME-summary-TEST.html

	mv logs/ghadoop-jobs.log logs/$NAME/$NAME-jobs.log
	mv logs/ghadoop-energy.log logs/$NAME/$NAME-energy.log
	mv logs/ghadoop-scheduler.log logs/$NAME/$NAME-scheduler.log
	mv logs/ghadoop-error.log logs/$NAME/$NAME-error.log

	# Plot results: energy
# 	echo "set term svg size 1280,600" > logs/$NAME/$NAME.plot
	echo "set term svg size 960,450" > logs/$NAME/$NAME.plot
	echo "set out \"logs/$NAME/$NAME-energy.svg\"" >> logs/$NAME/$NAME.plot
	echo "set ylabel \"Power (kW)\"" >> logs/$NAME/$NAME.plot
# 	echo "set yrange [0:1.8]" >> logs/$NAME/$NAME.plot
	echo "set yrange [0:2.4]" >> logs/$NAME/$NAME.plot

	echo "set y2label \"Brown energy price ($/kWh)\"" >> logs/$NAME/$NAME.plot
	echo "set y2range [0:0.3]" >> logs/$NAME/$NAME.plot
	echo "set y2tics" >> logs/$NAME/$NAME.plot
	echo "set ytics nomirror" >> logs/$NAME/$NAME.plot

	echo "set xdata time" >> logs/$NAME/$NAME.plot
	echo "set timefmt \"%s\"" >> logs/$NAME/$NAME.plot
	echo "set format x \"%a\n%R\"" >> logs/$NAME/$NAME.plot
	echo "set format x \"%a %R\"" >> logs/$NAME/$NAME.plot

	echo "set style fill solid" >> logs/$NAME/$NAME.plot
	echo "plot \"logs/$NAME/$NAME-energy.log\" using (\$1*24):(\$10/1000) lc rgb \"#808080\" w filledcurve title \"Brown consumed\",\\" >> logs/$NAME/$NAME.plot
	echo "\"logs/$NAME/$NAME-energy.log\" using (\$1*24):(\$8/1000) lc rgb \"#e6e6e6\" w filledcurve title \"Green consumed\",\\" >> logs/$NAME/$NAME.plot
# 	echo "\"logs/$NAME/$NAME-energy.log\" using (\$1+(2*24*24+10*24)):(\$2/1000) lw 2 lc rgb \"black\" w steps title \"Green predicted\",\\" >> logs/$NAME/$NAME.plot
	echo "\"logs/$NAME/$NAME-energy.log\" using (\$1*24):(\$3/1000) lw 2 lc rgb \"black\" w steps title \"Green predicted\",\\" >> logs/$NAME/$NAME.plot
	echo "\"logs/$NAME/$NAME-energy.log\" using (\$1*24):4 axes x1y2 lw 2 lc rgb \"black\" w steps title \"Brown price\"" >> logs/$NAME/$NAME.plot

	gnuplot logs/$NAME/$NAME.plot

	# Plot results: nodes
# 	echo "set term svg size 1280,600" > logs/$NAME/$NAME.plot
	echo "set term svg size 960,450" > logs/$NAME/$NAME-nodes.plot
	echo "set out \"logs/$NAME/$NAME-nodes.svg\"" >> logs/$NAME/$NAME-nodes.plot
	echo "set ylabel \"Nodes\"" >> logs/$NAME/$NAME-nodes.plot
	echo "set yrange [0:16]" >> logs/$NAME/$NAME-nodes.plot

	echo "set xdata time" >> logs/$NAME/$NAME-nodes.plot
	echo "set timefmt \"%s\"" >> logs/$NAME/$NAME-nodes.plot
	echo "set format x \"%a\n%R\"" >> logs/$NAME/$NAME-nodes.plot
	echo "set format x \"%a %R\"" >> logs/$NAME/$NAME-nodes.plot

	echo "set style fill solid" >> logs/$NAME/$NAME-nodes.plot
	echo "plot \"logs/$NAME/$NAME-energy.log\" using (\$1*24):7 lw 2 lc rgb \"#C0C0C0\" w filledcurve title \"Dec nodes\",\\" >> logs/$NAME/$NAME-nodes.plot
	echo "\"logs/$NAME/$NAME-energy.log\" using (\$1*24):6 lc rgb \"#909090\" w filledcurve title \"Up nodes\",\\" >> logs/$NAME/$NAME-nodes.plot
	echo "\"logs/$NAME/$NAME-energy.log\" using (\$1*24):5 lc rgb \"#404040\" w filledcurve title \"Run nodes\"" >> logs/$NAME/$NAME-nodes.plot

	gnuplot logs/$NAME/$NAME-nodes.plot
}

# DATE_MOST="2010-5-31T09:00:00"
# SOLAR_MOST="data/solarpower-31-05-2010" # Best energy
BROWN_SUMMER="data/browncost-onoffpeak-summer.nj"
BROWN_WINTER="data/browncost-onoffpeak-winter.nj"

# High High
DATE_1="2011-5-9T00:00:00"
# Abu Shoeb modified 0n 2016-07-26
SOLAR_1="data/solarpower-09-05-2011"
SOLAR_1_SHOEB="data/solarpower-09-05-2011-shoeb"
SOLAR_1_2K_3K="data/solarpower-09-05-2011-2k3k"
SOLAR_1_5k="data/solarpower-09-05-2011-5k"
BROWN_1=$BROWN_WINTER

# High Medium
DATE_2="2011-5-12T00:00:00"
SOLAR_2="data/solarpower-12-05-2011"
BROWN_2=$BROWN_WINTER

# Medium High
DATE_3="2011-6-14T00:00:00"
SOLAR_3="data/solarpower-14-06-2011"
BROWN_3=$BROWN_SUMMER

# Medium Medium
DATE_4="2011-6-16T00:00:00"
SOLAR_4="data/solarpower-16-06-2011"
BROWN_4=$BROWN_SUMMER

# Low Low
DATE_0="2011-5-15T00:00:00"
SOLAR_0="data/solarpower-15-05-2011"
BROWN_0=$BROWN_WINTER

PEAK_WINTER=5.5884 # Winter October-May
PEAK_SUMMER=13.6136 # Summer June-Sep

# Workloads
WORKLOAD_SHOEB_NEW="workload/workload-shoeb-new"
WORKLOAD_SHOEB="workload/shoeb"
WORKLOAD_WC_1="workload-shoeb/workload-wc-1"
WORKLOAD_WC_1_JOB="workload-shoeb/workload-wc-1-job"
WORKLOAD_WC_CONT_30_30JOBS="workload-shoeb/workload-wc-cont-30-30jobs"
WORKLOAD_WC_CONT_30_105_JOBS="workload-shoeb/workload-wc-cont-30-105jobs"
WORKLOAD_WC_CONT_30_105JOBS_HIGH="workload-shoeb/workload-wc-cont-30-105jobs-high"
WORKLOAD_WC_3_JOBS="workload-shoeb/workload-wc-3jobs"
WORKLOAD_WC_10_JOBS="workload-shoeb/workload-wc-10jobs"
WORKLOAD_WC_CONT_30_10JOBS="workload-shoeb/workload-wc-cont-30-10jobs"
WORKLOAD_WC_CONT_60_30_JOBS="workload-shoeb/workload-wc-cont-60-30jobs"
WORKLOAD_WC_CONT_60_105_JOBS="workload-shoeb/workload-wc-cont-60-105jobs"

TASK_ENERGY_015=0.30
TASK_ENERGY_030=0.35
TASK_ENERGY_060=0.45
TASK_ENERGY_090=0.55

TASK_ENERGY_NUTCH=0.14

REPLICATION=30

# Experiments start

# GreenOnly repeat
#experiment "jan31-wc1"          --energy --nobrown --peak 0.0 --repl $REPLICATION --taskenergy $TASK_ENERGY_030 -d $DATE_1 --speedup 24 --brownfile $BROWN_1 --greenfile $SOLAR_1 --pred -w $WORKLOAD_WC_1

# GreenVarPeak
#experiment "feb22-e1-30jobs"  --energy --peak $PEAK_SUMMER --repl $REPLICATION --taskenergy $TASK_ENERGY_030 -d $DATE_1 --speedup 24 --brownfile $BROWN_1 --greenfile $SOLAR_1 --pred -w $WORKLOAD_WC_CONT_30_30JOBS
#experiment "feb22-e2-105jobs"  --energy --peak $PEAK_SUMMER --repl $REPLICATION --taskenergy $TASK_ENERGY_030 -d $DATE_1 --speedup 24 --brownfile $BROWN_1 --greenfile $SOLAR_1 --pred -w $WORKLOAD_WC_CONT_30_105JOBS
#experiment "feb23-e1-105jobs-high"  --energy --peak $PEAK_SUMMER --repl $REPLICATION --taskenergy $TASK_ENERGY_030 -d $DATE_1 --speedup 24 --brownfile $BROWN_1 --greenfile $SOLAR_1 --pred -w $WORKLOAD_WC_CONT_30_105JOBS_HIGH
#experiment "feb23-e2-105jobs-high-summer"  --energy --peak $PEAK_SUMMER --repl $REPLICATION --taskenergy $TASK_ENERGY_030 -d $DATE_1 --speedup 24 --brownfile $BROWN_3 --greenfile $SOLAR_1 --pred -w $WORKLOAD_WC_CONT_30_105JOBS_HIGH
#experiment "feb23-e3-105jobs-summer"  --energy --peak $PEAK_SUMMER --repl $REPLICATION --taskenergy $TASK_ENERGY_030 -d $DATE_1 --speedup 24 --brownfile $BROWN_3 --greenfile $SOLAR_1 --pred -w $WORKLOAD_WC_CONT_30_105JOBS


#experiment "apr16-exp1"  --energy --peak $PEAK_SUMMER --repl $REPLICATION --taskenergy $TASK_ENERGY_030 -d $DATE_1 --speedup 24 --brownfile $BROWN_1 --greenfile $SOLAR_1_SHOEB --pred -w $WORKLOAD_WC_3_JOBS
#experiment "apr17-exp1"  --energy --peak $PEAK_SUMMER --repl $REPLICATION --taskenergy $TASK_ENERGY_030 -d $DATE_1 --speedup 24 --brownfile $BROWN_1 --greenfile $SOLAR_1 --pred -w $WORKLOAD_WC_CONT_30_105_JOBS
#experiment "apr17-exp2"  --energy --peak $PEAK_SUMMER --repl $REPLICATION --taskenergy $TASK_ENERGY_030 -d $DATE_1 --speedup 24 --brownfile $BROWN_1 --greenfile $SOLAR_1_SHOEB --pred -w $WORKLOAD_WC_10_JOBS
#experiment "apr17-exp3"  --energy --peak $PEAK_SUMMER --repl $REPLICATION --taskenergy $TASK_ENERGY_030 -d $DATE_1 --speedup 24 --brownfile $BROWN_1 --greenfile $SOLAR_1_2K_3K --pred -w $WORKLOAD_WC_CONT_60_30_JOBS
#experiment "apr17-exp4"  --energy --peak $PEAK_SUMMER --repl $REPLICATION --taskenergy $TASK_ENERGY_030 -d $DATE_1 --speedup 24 --brownfile $BROWN_1 --greenfile $SOLAR_1_2K_3K --pred -w $WORKLOAD_WC_CONT_60_105_JOBS
#experiment "apr18-exp1"  --energy --peak $PEAK_SUMMER --repl $REPLICATION --taskenergy $TASK_ENERGY_030 -d $DATE_1 --speedup 24 --brownfile $BROWN_1 --greenfile $SOLAR_1 --pred -w $WORKLOAD_WC_CONT_60_105_JOBS
experiment "apr18-exp2"  --energy --peak $PEAK_SUMMER --repl $REPLICATION --taskenergy $TASK_ENERGY_030 -d $DATE_1 --speedup 24 --brownfile $BROWN_1 --greenfile $SOLAR_1 --pred -w $WORKLOAD_WC_CONT_60_30_JOBS

#experiment "feb21-e1-105jobs-high-summer"  --energy --peak $PEAK_SUMMER --repl $REPLICATION --taskenergy $TASK_ENERGY_030 -d $DATE_1 --speedup 24 --brownfile $BROWN_4 --greenfile $SOLAR_1 --pred -w $WORKLOAD_CONT_WC_105JOBS_HIGH
# experiment "51-greenpeak-day2-30seconds-perfect"  --energy --peak $PEAK_SUMMER --repl $REPLICATION --taskenergy $TASK_ENERGY_030 -d $DATE_2 --speedup 24 --brownfile $BROWN_2 --greenfile $SOLAR_2 -w $WORKLOAD_CONT_030
# experiment "53-greenpeak-day4-30seconds-perfect"  --energy --peak $PEAK_SUMMER --repl $REPLICATION --taskenergy $TASK_ENERGY_030 -d $DATE_4 --speedup 24 --brownfile $BROWN_4 --greenfile $SOLAR_4 -w $WORKLOAD_CONT_030
# experiment "52-greenpeak-day3-30seconds-perfect"  --energy --peak $PEAK_SUMMER --repl $REPLICATION --taskenergy $TASK_ENERGY_030 -d $DATE_3 --speedup 24 --brownfile $BROWN_3 --greenfile $SOLAR_3 -w $WORKLOAD_CONT_030
# cp ghadoop.py.regular ghadoop.py
