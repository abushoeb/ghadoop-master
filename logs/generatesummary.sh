#!/bin/bash

echo "<head>"
echo "<title>Green Hadoop results</title>"
echo "</head>"

echo "<body>"
for file in `ls`; do
	if [ -f $file/$file-summary.log ]; then
		echo "<h2>$file</h2>"
		echo "<table>"
		echo "<tr>"
		echo "<td>"
		echo "<a href='$file/$file-energy.svg'><img src='$file/$file-energy.svg' width='600px'/></a><br/>"
		echo "<a href='$file/$file-nodes.svg'><img src='$file/$file-nodes.svg' width='600px'/></a><br/>"
		echo "</td>"
		echo "<td>"
		cat $file/$file-summary.log
		echo "</td>"
		echo "</tr>"
		echo "</table>"
		echo ""
		echo ""
	fi
done
echo "</body>"