set term svg size 960,450
set out "logs/greenonly-nov16-out2/greenonly-nov16-out2-energy.svg"
set ylabel "Power (kW)"
set yrange [0:2.4]
set y2label "Brown energy price ($/kWh)"
set y2range [0:0.3]
set y2tics
set ytics nomirror
set xdata time
set timefmt "%s"
set format x "%a\n%R"
set format x "%a %R"
set style fill solid
plot "logs/greenonly-nov16-out2/greenonly-nov16-out2-energy.log" using ($1*24):($10/1000) lc rgb "#808080" w filledcurve title "Brown consumed",\
"logs/greenonly-nov16-out2/greenonly-nov16-out2-energy.log" using ($1*24):($8/1000) lc rgb "#e6e6e6" w filledcurve title "Green consumed",\
"logs/greenonly-nov16-out2/greenonly-nov16-out2-energy.log" using ($1*24):($3/1000) lw 2 lc rgb "black" w steps title "Green predicted",\
"logs/greenonly-nov16-out2/greenonly-nov16-out2-energy.log" using ($1*24):4 axes x1y2 lw 2 lc rgb "black" w steps title "Brown price"
