# This script will copy input files to HDFS

echo "copying wc input files to hdfs"
hadoop dfs -copyFromLocal /home/shoeb/wc-big-16-files-same-size/ /home/shoeb/big1
sleep 15
hadoop dfs -copyFromLocal /home/shoeb/wc-big-16-files-same-size/ /home/shoeb/big2
sleep 15
hadoop dfs -copyFromLocal /home/shoeb/wc-big-16-files-same-size/ /home/shoeb/big3
