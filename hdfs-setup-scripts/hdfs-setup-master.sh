# This script will do a fresh hdfs setup 

cd ~
echo "stopping all hadoop services"
/home/shoeb/hadoop-0.21.0/bin/stop-all.sh

cd /app/hadoop/tmp/
echo "removing hadoop tmp data"
rm -R *
cd ~

cd /home/shoeb/hadoop-0.21.0/logs/
echo "removing hadoop log files"
rm -R *
cd ~

echo "setting up crypt02"
ssh crypt02 'bash -s' < /home/shoeb/ghadoop1/hdfs-setup-scripts/hdfs-setup-slaves.sh

echo "setting up crypt06"
ssh crypt06 'bash -s' < /home/shoeb/ghadoop1/hdfs-setup-scripts/hdfs-setup-slaves.sh

echo "formatting namenode"
hadoop namenode -format

echo "starting hdfs"
/home/shoeb/hadoop-0.21.0/bin/start-dfs.sh

echo "starting hdfs"
/home/shoeb/hadoop-0.21.0/bin/start-mapred.sh
