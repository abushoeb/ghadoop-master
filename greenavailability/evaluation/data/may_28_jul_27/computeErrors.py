#!/usr/bin/python
import os,sys,re,string,getopt,glob,math
totalHoursInOneDay = None
totalHours = 5*24-9
from datetime import *

noofmethods = 3
error_corr_count = [0, 0, 0]
probable_count = [0 , 0, 0]
#horizon = [1,3,6,12,24,48]

def parseErrorForFile(filename,horizon=49,long=False):
	global totalHoursInOneDay
	startTime=None    
	endTime = None    
	totalActual = 0    
	totalPredicted = []    
	totalErrorProduction = []    
	count = 0    
	errorSum = []    
	weightedSum=[]    
	actualCount=0    
	predictedCount=[]    
	done = [False, False, False]
	state = [0,0,0]    
	


	for i in range(noofmethods):
		totalPredicted.append(0)
		totalErrorProduction.append(0)
		errorSum.append(0)
		weightedSum.append(0)	
		predictedCount.append(0)

	fd = open(filename,'r')
	call_hour = int(filename.split(".")[1].split("_")[-1])
	#print call_hour
	if call_hour<8 or call_hour>17:
		for i in range(noofmethods):
			state[i]=2
    
	if long:
		totalHoursInOneDay=48
	else:
		totalHoursInOneDay= 24
        
    i = -1
	for line in fd:
		line = line.strip()
		if not line or line.startswith("#"):
			continue

    
		elements = line.split()
        
		if startTime == None:
			if long:
				startTime = elements[0]+" "+elements[1]
			else:
				startTime = elements[0]

		predicted = []
	#error = []

	#errorRate = []
        #weightedRate = []

		hour = int(elements[3])
		actual = float(elements[7])
		base = float(elements[8])
		tag = float(elements[9])
		for i in range(noofmethods):
			predicted.append(float(elements[i+4]))
			error = (abs(predicted[i]-actual))
			if abs(predicted[i]-base*tag/100)>0.5 and not done[i]:
				#print base, tag, predicted[i], base*tag/100
				error_corr_count[i] += 1
				#done[i] = True
			if hour>8:
				#print state[i], call_hour,hour
				if state[i]==0:
					probable_count[i] += 1
					state[i] = 1
				elif state[i]==1:
					probable_count[i] += 1
			elif state[i]==1:
				state[i] = 2

			errorRate = 0
			weightedRate = 0
        
			if(actual>0):
				errorRate = error/actual
				weightedRate = errorRate*actual            
				totalErrorProduction[i]+=actual	       	
			elif predicted>0:
				errorRate = error/predicted[i]        
				weightedRate = errorRate*predicted[i]        	
				totalErrorProduction[i]+=predicted[i]        	
				predictedCount[i]+=1        	
	
			errorSum[i]+=errorRate
			weightedSum[i]+=weightedRate
			totalPredicted[i]+=predicted[i]
		if actual>0:		
			actualCount+=1
        
                
		totalActual+=actual
		count+=1
		
	fd.close()

	#print filename, totalPredicted, totalActual, totalHoursInOneDay, count
	
	avgError=[]
	avgWeightedError=[]
	productionError=[]
	avgErrorIncludeingNights=[]
	for i in range(noofmethods):
		avgError.append(errorSum[i]/count*100)
		avgWeightedError.append(weightedSum[i]/totalErrorProduction[i]*100)
		productionError.append(abs(totalPredicted[i]-totalActual)/totalActual*100)
		avgErrorIncludeingNights.append(errorSum[i]/totalHoursInOneDay*100)

	return (avgErrorIncludeingNights,avgError,avgWeightedError,productionError,count,actualCount,predictedCount,startTime)



if __name__=='__main__':

    commonOptions="f:d:hl"
    opts, args = getopt.getopt(sys.argv[1:], commonOptions)
    printHtml=False
    twodays = False
    files = []
    
    for o,a in opts:
        if o == '-b':
            a = a.strip()
            d = datetime.strptime(a,format)
            BASE_DATE = datetime(d.year,d.month,d.day)
        elif o == '-d':
            dirname = a
            files.extend(glob.glob(os.path.join(dirname,"data*.txt")))
        elif o == '-f':
            files.extend(a.split(","))
        elif o == "-l":
            twodays = True
        elif o == "-f":
            shift = int(a)
            
    
    files.sort()
    filecount = 1
    
    if printHtml:
        print "<table border='2'>"
        #print "<tr><th>Day/Time</th><th>Avg Error(24 hours)</th><th>Avg Error(nonzero Actual or Prediction)</th><th>Weighted Average with Production</th><th>Daily Production Error</th></tr>"
	print "<tr><th>Day/Time</th><th>Avg Error(24 hours)</th><th>Avg Error(24 hours)</th><th>Avg Error(24 hours)</th><th>Avg Error(nonzero Actual)</th><th>Avg Error(nonzero Actual)</th><th>Avg Error(nonzero Actual)</th><th>Weighted Error</th><th>Weighted Error</th><th>Weighted Error</th><th>Daily Production Error</th><th>Daily Production Error</th><th>Daily Production Error</th></tr>"
	print "<tr><td></td><td>Normal</td><td>Threshold</td><td>No threshold</td><td>Normal</td><td>Threshold</td><td>No threshold</td><td>Normal</td><td>Threshold</td><td>No threshold</td><td>Normal</td><td>Threshold</td><td>No threshold</td></tr>"
    for f in files:
        (avgErrorIncludeingNights,avgError,avgWeightedError,productionError,count,actualCount,predictedCount,day) = parseErrorForFile(f,shift,long=twodays)
        str1 = ""
	if printHtml:
		str1 += "<td>"+"<a href=\"#fig%d\">"%filecount+day+"</a></td>"
		filecount += 1
	else:
		str1 += day+"\t"
	for i in range(noofmethods):
  		if printHtml:
        	    #print "<tr><td>%s</td><td>%.2f</td><td>%.2f</td><td>%.2f</td><td>%.2f</td></tr>"%(day,avgErrorIncludeingNights,avgError,avgWeightedError,productionError)
			str1 += "<td>%.2f</td>"%(avgErrorIncludeingNights[i])
        	else:
        	    #print f,"%.2f\t%.2f\t%.2f\t%.2f\t%d\t%d\t%d"%(avgErrorIncludeingNights[i],avgError[i],avgWeightedError[i],productionError[i],count,actualCount,predictedCount[i])
			str1 += "%.2f\t"%(avgErrorIncludeingNights[i])
	for i in range(noofmethods):
  		if printHtml:
			str1 += "<td>%.2f</td>"%(avgError[i])
        	else:
			str1 += "%.2f\t"%(avgError[i])
	for i in range(noofmethods):
  		if printHtml:
			str1 += "<td>%.2f</td>"%(avgWeightedError[i])
        	else:
			str1 += "%.2f\t"%(avgWeightedError[i])
	for i in range(noofmethods):
  		if printHtml:
			str1 += "<td>%.2f</td>"%(productionError[i])
        	else:
			str1 += "%.2f\t"%(productionError[i])

	for i in range(noofmethods):
		if not printHtml:
			str1 += "%d\t%d\t"%(count,predictedCount[i])
	if printHtml:
		str1 += "</tr>"
	print str1#, error_corr_count[0], error_corr_count[1], error_corr_count[2], probable_count[0], probable_count[1], probable_count[2]
        
    if printHtml:
        print "</table>"
    	files = glob.glob(dirname+"/../*.png")        
    	files.sort()
    imgcount = 1
    i = 0
    if printHtml:    
		print "<table border='2'>"
		for f in files:
			if i%2==0: #on every label we have two files, one prediction and another tag
				print "<a name=fig%d>\n"%(imgcount)
				imgcount += 1
		print "<tr><img src=\"%s\" alt=\"%s\"></img></tr>\n"%(f,f)
		i += 1
		print "</table>"
		if imgcount<>filecount:
			print imgcount, filecount
