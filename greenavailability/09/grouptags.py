#!/usr/bin/python

import sys

conditions = {
	'mostly sunny': 2,
	'mostly sunny / wind': 2,
	'mostly sunny/wind': 2,
	'sunny': 1,
	'sunny / wind': 1,
	'sunny and windy': 1,
	'clear': 1,
	'clear / wind': 1,
	'clear/wind': 1,
	'mostly clear': 2,
	'mostly clear / wind': 2,
	'mostly clear/wind': 2,
	'fair': 3,
	'fair and windy': 3,
	'partly cloudy': 4,
	'partly cloudy / wind': 4,
	'partly cloudy and windy': 4,
	'partly cloudy/wind': 4,
	'flurries': 5,
	'flurries / wind': 5,
	'flurries/wind': 5,
	'mostly cloudy': 6,
	'mostly cloudy / wind': 6,
	'mostly cloudy and windy': 6,
	'mostly cloudy/wind': 6,
	'few showers': 7,
	'light freezing rain': 7,
	'light freezing rain/sleet': 7,
	'light rain': 7,
	'light rain and fog': 7,
	'light rain and freezing rain': 7,
	'light rain and windy': 7,
	'light rain with thunder': 7,
	'light rain/fog': 7,
	'rain': 8,
	'rain and freezing rain': 8,
	'rain shower': 8,
	'showers': 8,
	'wintry mix': 8,
	'few snow showers': 9,
	'few snow showers / wind': 9,
	'light snow': 9,
	'light snow / wind': 9,
	'light snow and fog': 9,
	'light snow and windy': 9,
	'light snow shower': 9,
	'rain and snow': 9,
	'cloudy': 10,
	'cloudy / wind': 10,
	'cloudy and windy': 10,
	'cloudy/wind': 10,
	'snow': 11,
	'snow / wind': 11,
	'snow and fog': 11,
	'snow shower': 11,
	'snow shower / wind': 11,
	'snow/blowing snow': 11,
	'snow/wind': 11,
	'drizzle and fog': 12,
	'fog': 12,
	'foggy': 12,
	'haze': 12,
	'heavy rain': 13,
	'heavy t-storm': 7,
	'isolated t-storms': 7,
	'isolated t-storms / wind': 7,
	'scattered strong storms': 7,
	'scattered strong storms / wind': 7,
	'scattered t-storms': 7,
	'scattered t-storms / wind': 7,
	'squalls': 7,
	'squalls and windy': 7,
	'strong storms': 7,
	'strong storms / wind': 7,
	't-showers': 8,
	't-storm': 7,
	't-storms': 7,
	'thunder': 7,
	'thunder in the vicinity': 7,
	'blizzard': 13,
	'blowing snow and windy': 13,
	'heavy snow': 13,
	'heavy snow / wind': 13
	}



#print conditions.values()

inp = open(sys.argv[1],"r")
outp = open("wea_data.txt","w")
for line in inp.readlines():
	list1 = line.split("\t")
	list1[5] = str(conditions[list1[6]])
	str1 = ""
	for j in range(len(list1)):
		str1 += list1[j]
		str1 += "\t"

	outp.write(str1.strip()+"\n")	
inp.close()
outp.close()
		


