#!/usr/bin/env python2.7

# Abu Shoeb, Computer Science, Rutgers University
# October 13, 2016

import threading, time
from ghadoopcommons import *

import math
import time
import sys
import os
import threading
import string
import random
import signal
import subprocess
import socket

from operator import itemgetter

from datetime import datetime,timedelta
from subprocess import call, PIPE, Popen

from ghadoopdata import *

USER = "shoeb"
HADOOP_HOME = "/home/"+USER+"/hadoop-0.21.0"
SLAVES_SHARED = HADOOP_HOME+"/conf/slaves-shared"
SLAVES_MASTER = HADOOP_HOME+"/conf/slaves"

CMD_HADOOP_REPORT="hadoop dfsadmin -report"
CMD_HADOOP_EXAMPLE = "hadoop jar "+HADOOP_HOME+"/hadoop-mapred-examples-0.21.0.jar wordcount /home/shoeb/wc-in-101 /home/shoeb/wc-out-101-jul14-3"
CMD_STOP_TASKTRACKER = HADOOP_HOME+"/bin/hadoop-daemon.sh stop tasktracker"
CMD_START_TASKTRACKER = HADOOP_HOME+"/bin/hadoop-daemon.sh start tasktracker"

# This method removes a Node ID from the file
def removeNodeIdFromFile(nodeId, fileName):
	f = open(fileName,"r")
	lines = f.readlines()
	f.close()

	f = open(fileName,"w")
	for line in lines:
		if line!=nodeId+"\n":
			f.write(line)
	f.close()

# This method adds a Node ID to the file
def addNodeIdToFile(nodeId, fileName):
	f = open(fileName,"a")
	f.write(nodeId+'\n')
	f.close()

def getSharedSlaves():
	ret = []
	file=open(SLAVES_SHARED, "r")
	for line in file:
		name=line.replace("\n", "")
		if name != "":
			if name not in ret:
				ret.append(name)
	return ret

def getCurrentStatus(nodeId, service):
	
	if service == "hdfs":
		if isOpen(nodeId, PORT_HDFS):
			ret = "UP"
		else:
			ret = "DOWN"
	
	elif service == "mapred":
		if isOpen(nodeId, PORT_MAPRED):
			ret = "UP"
		else:
			ret = "DOWN"		
	
	return ret

def setNodeStatusUp(nodeId):
	# Turn on mapred
	current = getCurrentStatus(nodeId, "mapred")
	while current=="DOWN":
		exit = call([HADOOP_HOME+"/bin/manage_node.sh", "start", "mapred", nodeId], stdout=open('/dev/null', 'w'), stderr=open('/dev/null', 'w'))
		current = getCurrentStatus(nodeId, "mapred")
	
	# Turn on hdfs
	current = getCurrentStatus(nodeId, "hdfs")
	while current=="DOWN":
		exit = call([HADOOP_HOME+"/bin/manage_node.sh", "start", "hdfs", nodeId], stdout=open('/dev/null', 'w'), stderr=open('/dev/null', 'w'))
		current = getCurrentStatus(nodeId, "hdfs")

def test(nodeId1):
	requiredFiles = getRequiredFiles()
	decNodes = []
	decNodes.append(nodeId1)
	#for nodeId in reversed(decNodes):
	for nodeId in decNodes:
		# DEC->DOWN Turn off decommission nodes:
		#   MapReduce: Hasn't executed tasks of running jobs
		#   HDFS:      All its required data available in UP nodes
		print nodeTasks[nodeId]
		if len(nodeTasks[nodeId])==0 and len(nodeJobs[nodeId])==0:
			print "1"
			auxFiles=[]
			required = False
			# Get the number of required files of the node
			for fileId in nodeFile.get(nodeId, []):
				if fileId not in auxFiles and fileId in requiredFiles:
					auxFiles.append(fileId)
			for fileId in auxFiles:
				file = files[fileId]
				missing = True
				for otherNodeId in file.getLocation():
					if nodeId != otherNodeId:
						if otherNodeId in ALWAYS_NODE or otherNodeId in onNodes or otherNodeId in decNodes:
							missing = False
							break
				if missing:
					required = True
					break
			if not required:
				print "\tTurn off "+nodeId
				#if getDebugLevel() > 1:
					#print "\tTurn off "+nodeId
				#thread = threading.Thread(target=setNodeStatus,args=(nodeId, False))
				#thread.start()
				#threads["dec->off "+nodeId] = thread
				# Update data structure
				#offNodes.append(nodeId)
				#decNodes.remove(nodeId)	
									
if __name__=='__main__':
	# Shared node request from outside i.e. Green Cassandra
	# requestNode = False
	# get the shared node request via command line argument 
	if len(sys.argv)>1:
		requestNode = str(sys.argv[1])
	else:
		print "no argument"
		test('crypt02')
		exit()
	
	for i in range(0,1):
		if requestNode == "yes":
			print "Removing a node"
			nodes = getNodes()
			for nodeId in sorted(nodes):
				print "\t"+str(nodeId)+":\t"+str(nodes[nodeId][0])+"\t"+str(nodes[nodeId][1])
				#if nodeId == "crypt02" and (nodes[nodeId][0]=="UP" or nodes[nodeId][1]=="UP"):
				if nodeId == "crypt02" and nodes[nodeId][1]=="UP":
					print nodeId+" HDFS : Up --> Dec"
					#setNodeDecommission(nodeId, True)
					setNodeHdfsDecommission(nodeId, True)
					print nodeId+" HDFS : Dec --> Down"
					#setNodeStatus(nodeId, False)
					setNodeHdfsStatus(nodeId, False)
					removeNodeIdFromFile(nodeId,SLAVES_MASTER)
					addNodeIdToFile(nodeId,SLAVES_SHARED)
					
				if nodeId == "crypt02" and (nodes[nodeId][0]=="DEC" or nodes[nodeId][1]=="DEC"):
					print nodeId+" : Dec --> Down"
					setNodeStatus(nodeId, False)
					removeNodeIdFromFile(nodeId,SLAVES_MASTER)
					addNodeIdToFile(nodeId,SLAVES_SHARED)
				
				'''if nodeId == "crypt02" and (nodes[nodeId][0]=="DOWN" and nodes[nodeId][1]=="DOWN"):
					print nodeId+" : Down --> Remove"
					removeNodeIdFromFile(nodeId,SLAVES_MASTER)
					addNodeIdToFile(nodeId,SLAVES_SHARED)'''
		
		if requestNode == "no":
			shared_slaves = getSharedSlaves()
			if len(shared_slaves) == 0:
				print "No nodes to return"
				exit()
			for nodeId in shared_slaves:
				print "Returning node : "+str(nodeId)
				print "Removing "+str(nodeId)+" from SLAVES_SHARED"
				removeNodeIdFromFile(nodeId,SLAVES_SHARED)
				print "Adding "+str(nodeId)+" to SLAVES_MASTER"
				addNodeIdToFile(nodeId,SLAVES_MASTER)
				print nodeId+" : Down --> Up"
				setNodeStatusUp(nodeId)
				print "Done"		
