#!/usr/bin/env python2.7

# Abu Shoeb, Computer Science, Rutgers University
# October 13, 2016

import threading, time
from ghadoopcommons import *

import math
import time
import sys
import os
import threading
import string
import random
import signal
import subprocess
import socket

from operator import itemgetter

from datetime import datetime,timedelta
from subprocess import call, PIPE, Popen

from ghadoopdata import *

SHARED_SLAVES_FILE = "shared-slaves"
MASTER_SLAVES_FILE = "master-slaves"
USER = "shoeb"
HADOOP_HOME = "/home/"+USER+"/hadoop-0.21.0"

CMD_HADOOP_REPORT="hadoop dfsadmin -report"
CMD_HADOOP_EXAMPLE = "hadoop jar "+HADOOP_HOME+"/hadoop-mapred-examples-0.21.0.jar wordcount /home/shoeb/wc-in-101 /home/shoeb/wc-out-101-jul14-3"
CMD_STOP_TASKTRACKER = HADOOP_HOME+"/bin/hadoop-daemon.sh stop tasktracker"
CMD_START_TASKTRACKER = HADOOP_HOME+"/bin/hadoop-daemon.sh start tasktracker"

# This method removes a Node ID from the file
def removeNodeIdFromFile(nodeId, fileName):
	f = open(fileName,"r")
	lines = f.readlines()
	f.close()

	f = open(fileName,"w")
	for line in lines:
		if line!=nodeId+"\n":
			f.write(line)
	f.close()

# This method adds a Node ID to the file
def addNodeIdToFile(nodeId, fileName):
	f = open(fileName,"a")
	f.write(nodeId+'\n')
	f.close()

def doSsh(host,command):
	ssh = subprocess.Popen(["ssh", "%s" % host, command],
						   shell=False,
						   stdout=subprocess.PIPE,
						   stderr=subprocess.PIPE)
	result = ssh.stdout.readlines()
	if result == []:
		#error = ssh.stderr.readlines()
		#print >>sys.stderr, "ERROR: %s" % error
		return False
	else:
		#print result
		return True

if __name__=='__main__':
	
	#setNodeMapredStatus(nodeId, running, decommissioned=False)

	threads = {}
	for i in range(0,1):
		nodes = getNodes()
		changed = False
		#nodeId = "crypt02"
		# Turn on
		#current = getNodeMapredStatus(nodeId)
		#while not changed and current=="DOWN":
			#exit = call([HADOOP_HOME+"/bin/manage_node.sh", "start", "mapred", nodeId], stdout=open('/dev/null', 'w'), stderr=open('/dev/null', 'w'))
			#current = getNodeMapredStatus(nodeId)
			#changed = True
		
		for nodeId in sorted(nodes):
			print "\t"+str(nodeId)+":\t"+str(nodes[nodeId][0])+"\t"+str(nodes[nodeId][1])
			if nodeId == "crypt02":
				print "Up --> Dec"
				setNodeDecommission(nodeId, True)
				setNodeStatus(nodeId, False)
				#exit = call([HADOOP_HOME+"/bin/manage_node.sh", "stop", "mapred", nodeId], stdout=open('/dev/null', 'w'), stderr=open('/dev/null', 'w'))
				#setNodeMapredStatus(nodeId, False)
				#setNodeHdfsStatus(nodeId, False)
				#changed = True
			'''if nodeId == "crypt02" and (nodes[nodeId][0]== "UPc" or nodes[nodeId][1]== "UPc") and not changed:
			if nodeId == "crypt02" and (nodes[nodeId][0]== "DEC" or nodes[nodeId][1]== "DEC") and not changed:
				print "Dec --> Down"
				changed = True	
			if nodeId == "crypt02" and (nodes[nodeId][0]== "DOWN" or nodes[nodeId][1]== "DOWN") and not changed:
				print "Down --> Up"
				setNodeMapredStatus(nodeId, True)
				#setNodeHdfsStatus(nodeId, True)
				changed = True'''	
			'''if nodeId == "crypt02" and (nodes[nodeId][0]== "DEC" or nodes[nodeId][1]== "DEC") and not changed:
				print "Dec --> Down"
				changed = True
			if nodeId == "crypt02" and (nodes[nodeId][0]== "DOWN" or nodes[nodeId][1]== "DOWN") and not changed:	
				print "Down --> Up"
				changed = True'''
			'''if nodeId == "crypt14":
				setNodeMapredStatus(nodeId, True)'''
			time.sleep(3)	
		print "\n"
		#time.sleep(2)
		# Remove a nodeId if both MapRed and HDFS are DOWN 
		#removeNodeIdFromFile("crypt02",MASTER_SLAVES_FILE)
		# Add recently removed NodeId to shared slaves list
		#addNodeIdToFile("crypt02",SHARED_SLAVES_FILE)
		#removeNodeIdFromFile("crypt04",MASTER_SLAVES_FILE)
		#addNodeIdToFile("crypt04",SHARED_SLAVES_FILE)
	

