#!/usr/bin/python

# Abu Shoeb, Computer Science, Rutgers University

import time

TIME_LIMIT = 2*60 # 1 min
#TIME_LIMIT = 20 # 20 sec
USER = "shoeb"
HADOOP_HOME = "/home/"+USER+"/hadoop-0.21.0"
PORT_MAPRED = 50060
PORT_HDFS = 50075
HADOOP_DECOMMISSION_MAPRED = HADOOP_HOME+"/conf/disabled_nodes"
HADOOP_DECOMMISSION_HDFS = HADOOP_HOME+"/conf/disabled_nodes_hdfs"
HADOOP_OFF_HDFS = HADOOP_HOME+"/conf/disabled_nodes_off"
TEST_FILE = "fromgc.txt"

if __name__=='__main__':

	timeElapsed = 0
	start_time = int(time.time())
	
	while timeElapsed<TIME_LIMIT:
		timeElapsed = int(time.time() - start_time)
		print "Time Elapsed : "+str(timeElapsed)
		result1 = open(TEST_FILE).read()
		print "Value from file : "+result1
		time.sleep(10)
